FROM node:lts as build

WORKDIR /app

COPY angular8/ .

RUN npm install && npm run build

FROM nginx:latest

WORKDIR /usr/share/nginx/html

COPY --from=build /app/dist/project-base .

EXPOSE 80